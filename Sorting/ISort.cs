﻿using System;
using System.Collections.Generic;

namespace Sorting
{
	public interface ISort
	{
		/// <summary>
		/// The simpleiest sort
		/// </summary>
		/// <param name="source">source list</param>
		/// <param name="compareFunc">1 if first gt then second, -1 if first lt then second, 0 if equal</param>
		/// <returns></returns>
		IEnumerable< MyData > Sort( IEnumerable< MyData > source, Func< MyData, MyData, int > compareFunc );
	}
}