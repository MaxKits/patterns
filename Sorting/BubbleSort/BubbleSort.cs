﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sorting.BubbleSort
{
	public class BubbleSort: ISort
	{
		/// <summary>
		/// The simpleiest sort
		/// </summary>
		/// <param name="source">source list</param>
		/// <param name="compareFunc">1 if first gt then second, -1 if first lt then second, 0 if equal</param>
		/// <returns></returns>
		public IEnumerable< MyData > Sort( IEnumerable< MyData > source, Func< MyData, MyData, int > compareFunc )
		{
			var sourceArray = source.ToArray();
			for( var i = 0; i < sourceArray.Count() - 1; i++ )
			{
				for( var j = i + 1; j < sourceArray.Count(); j++ )
				{
					if( compareFunc( sourceArray[ i ], sourceArray[ j ] ) == 1 )
					{
						var temp = sourceArray[ i ];
						sourceArray[ i ] = sourceArray[ j ];
						sourceArray[ j ] = temp;
					}
				}
			}

			return sourceArray;
		}
	}
}