﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sorting.QuickSort
{
	public class QuickSort: ISort
	{
		/// <summary>
		/// The simpleiest sort
		/// </summary>
		/// <param name="source">source list</param>
		/// <param name="compareFunc">1 if first gt then second, -1 if first lt then second, 0 if equal</param>
		/// <returns></returns>
		public IEnumerable< MyData > Sort( IEnumerable< MyData > source, Func< MyData, MyData, int > compareFunc )
		{
			var sourceList = source as IList< MyData > ?? source.ToList();
			if( !sourceList.Any() )
				return Enumerable.Empty< MyData >();

			var pivot = sourceList.First();
			var smaller = sourceList.Skip( 1 ).Where( item => compareFunc( item, pivot ) <= 0 );
			var smallerSorted = this.Sort( smaller, compareFunc );
			var larger = sourceList.Skip( 1 ).Where( item => compareFunc( item, pivot ) > 0 );
			var largerSorted = this.Sort( larger, compareFunc );

			return smallerSorted.Concat( new[] { pivot } ).Concat( largerSorted );
		}
	}

	public static class SortingHelper
	{
		public static IEnumerable< MyData > QuickSorting( this IEnumerable< MyData > source, Func< MyData, MyData, int > compareFunc )
		{
			var sourceList = source as IList< MyData > ?? source.ToList();
			if( !sourceList.Any() )
				return Enumerable.Empty< MyData >();

			var pivot = sourceList.First();
			var smallerSorted = sourceList.Skip( 1 ).Where( item => compareFunc( item, pivot ) <= 0 ).QuickSorting(compareFunc);
			var largerSorted = sourceList.Skip( 1 ).Where( item => compareFunc( item, pivot ) > 0 ).QuickSorting(compareFunc);

			return smallerSorted.Concat( new[] { pivot } ).Concat( largerSorted );
		}
	}
}