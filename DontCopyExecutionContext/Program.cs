﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DontCopyExecutionContext
{
	class Program
	{
		static void Main(string[] args)
		{
			CallContext.LogicalSetData("LogicalSetDataName", "Jeffery");
			CallContext.SetData("SetDataName", "Jeffery");
			ThreadPool.QueueUserWorkItem(state => Console.WriteLine("LogicalSetDataName={0}, SetDataName={1}", CallContext.LogicalGetData("LogicalSetDataName"), CallContext.GetData("SetDataName")));
			ExecutionContext.SuppressFlow();
			ThreadPool.QueueUserWorkItem(state => Console.WriteLine("LogicalSetDataName={0}, SetDataName={1}", CallContext.LogicalGetData("LogicalSetDataName"), CallContext.GetData("SetDataName")));
			ExecutionContext.RestoreFlow();
			Console.ReadLine();
		}
	}
}
