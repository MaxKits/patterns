﻿module AzureStorageCleaner
open FSharp.Azure.StorageTypeProvider
open Microsoft.WindowsAzure.Storage.Blob

type Azure = AzureTypeProvider<"DefaultEndpointsProtocol=https;AccountName=svtemp;AccountKey=tywvZxNJbDZhbk9iPRnItvt3kYA9rdyvY0Gsk5wfUPGPmweo9UE3lA1PNQmlyzbd0TolTeikYJVqWck/SfZ2gA==;">

type BlobNameInfo(integrationName, year, month, day) = 
    member this.IntegrationName = integrationName
    member this.Year = year
    member this.Month = month  
    member this.Day = day  
    member this.LogDateTime = new System.DateTime( year, month, day)  

type BlobAndBlobNameInfo(blobNameInfo:BlobNameInfo, iListBlobItem:IListBlobItem) = 
    member this.BlobNameInfo = blobNameInfo
    member this.IListBlobItem = iListBlobItem

let fprintfnTSafe  (writer:System.IO.StreamWriter)  (monitor:obj)(str:string) = 
    lock monitor (fun () -> printfn "%s" str;fprintfn writer "%s" str;writer.Flush())

let getBlobWithInfo (listBlobItem:IListBlobItem ) = 
    let parseNameParts (listBlobItem:IListBlobItem ) = 
        let blobUriParts = listBlobItem.Uri.ToString().Split '/'
        let blobUriPartsCount = Array.length blobUriParts
        new BlobNameInfo(blobUriParts.[blobUriPartsCount-2],System.Int32.Parse <| blobUriParts.[blobUriPartsCount-1].Substring(0,4),System.Int32.Parse <|blobUriParts.[blobUriPartsCount-1].Substring(4,2),System.Int32.Parse <|blobUriParts.[blobUriPartsCount-1].Substring(6,2))
    try
        Some <| parseNameParts listBlobItem 
    with
        | :? System.ArgumentException -> None
        | _ -> None

let processInBatchAsync2 a b c d =  Netco.Extensions.EnumerableExtensions.DoInBatchAsync <| d a b c
let processInBatchAsync3 process someSeq =  Netco.Extensions.EnumerableExtensions.DoInBatchAsync <| someSeq 100 process

let genLogRecord2 len uri (spc:int64) = sprintf "%s Delete\t%14d:\tblock blob of length %10d: %O " (System.DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")) spc len uri

let findFiles = 
    use file = System.IO.File.AppendText <| "c:/temp/" + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")+"-cleanazure.txt"
    let savedSpace2:Ref<int64> = ref <| int64 0 
    let monitor = new obj()
    file.AutoFlush <-true
   // fprintfnTSafe file "test" monitor

    let blobItemProcessor (blobI:BlobAndBlobNameInfo)=                                                                
        match blobI.IListBlobItem with 
            | :? CloudBlockBlob as blob -> 
                System.Threading.Interlocked.Add (savedSpace2, blob.Properties.Length) |> genLogRecord2 blob.Properties.Length blob.Uri |> fprintfnTSafe file monitor|> ignore
                blob.DeleteIfExists()
            | :? CloudPageBlob as pageBlob ->
                System.Threading.Interlocked.Add(savedSpace2 ,pageBlob.Properties.Length) |>genLogRecord2 pageBlob.Properties.Length pageBlob.Uri |> fprintfnTSafe file monitor|> ignore
                pageBlob.DeleteIfExists()
            | :? CloudBlobDirectory as directory    ->
                printfn "!!!Skip directory: %O" directory.Uri
                false
            | _ ->
                printfn "Unknown blob type: %O" (blobI.IListBlobItem.GetType())
                false    
    let blobItemProcessor2 (blobI:obj) =                                                                
        match blobI with 
            | :? BlobAndBlobNameInfo as blob -> 
                blobItemProcessor blob
            | _ ->
                printfn "Unknown blob type: %O" (blobI.GetType())
                false
    let blobItemProcessorTask (blobI:BlobAndBlobNameInfo)= System.Threading.Tasks.Task.Factory.StartNew<bool> ( blobItemProcessor2, blobI)
                
    let DoInBatchAsync1 _threads _processor _seq =  Netco.Extensions.EnumerableExtensions.ProcessInBatchAsync( _seq,_threads,_processor)
    let DoInBatchAsync2 _processor _seq = DoInBatchAsync1 200 _processor _seq
                
    let theBlobs =  Azure.Containers.``sv-shippedlogs``.AsCloudBlobContainer().ListBlobs("",true) 
                                                                |> Seq.map (fun b -> (b, getBlobWithInfo b)) 
                                                                |> Seq.where (fun (blob,blobI) -> blobI.IsSome ) 
                                                                |> Seq.map (fun (blob,blobI) -> new BlobAndBlobNameInfo( blobI.Value, blob) ) 
                                                                |> Seq.where (fun (blobWideInf)->(System.DateTime.UtcNow - blobWideInf.BlobNameInfo.LogDateTime).TotalDays>90.0 && blobWideInf.BlobNameInfo.IntegrationName <> "LogsToBlobService" ) 
                                                                //|> processInBatchAsync2 100 (fun o -> System.Threading.Tasks.Task.Factory.StartNew<bool>( fun () -> blobItemProcessor o)) true
                                                                |> DoInBatchAsync2 (new System.Func<BlobAndBlobNameInfo,System.Threading.Tasks.Task<bool>>( blobItemProcessorTask))
    
    let res = theBlobs.Result
    //let processInBatchAsync4 someSeq =  Netco.Extensions.EnumerableExtensions.DoInBatchAsync <| someSeq 100 (fun o -> System.Threading.Tasks.Task.Factory.StartNew<bool>( fun () -> blobItemProcessor o))
    0
    