﻿using System;

namespace Observer
{
	public class Director: IObserver
	{
		private readonly string _directorName;
		private ConsoleColor ForegroundColor{ get; set; }

		public Director( string directorName, ConsoleColor color )
		{
			if( directorName == null )
				throw new ArgumentNullException( "directorName" );
			this._directorName = directorName;
			this.ForegroundColor = color;
		}

		public void When( WorkStarted workStarted )
		{
			this.RecordToWorkLog( workStarted.Name, workStarted.WorkDescription );
		}

		public void When( WorkEnded workEnded )
		{
			this.RecordToWorkLog( workEnded.Name, workEnded.WorkDescription );
		}

		public void RecordToWorkLog( string name, string description )
		{
			Console.ForegroundColor = this.ForegroundColor;
			Console.WriteLine( "{3}:\t{0} - {1};\t{2}", DateTime.Now, name, description, this._directorName );
		}
	}
}