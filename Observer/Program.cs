﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Observer
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			var e1 = new Employer( "John" );
			var e2 = new Employer( "Andy" );
			var e3 = new Employer( "Alex" );

			var d1 = new Director( "Tom", ConsoleColor.Yellow );
			var d2 = new Director( "Bob", ConsoleColor.Green );

			e1.AddObserver( d1 );
			e2.AddObserver( d1 );
			e3.AddObserver( d1 );

			e3.AddObserver( d2 );

			var tasks = new List< Task >();
			var random = new Random( 100 );

			tasks.Add( new TaskFactory().StartNew( async () =>
			{
				await Task.Delay( 5000 );
				e1.StartCleanGarden();
			} ) );
			tasks.Add( new TaskFactory().StartNew( async () =>
			{
				await Task.Delay( random.Next( 3000, 15000 ) );
				e2.StartSleeping();
			} ) );
			tasks.Add( new TaskFactory().StartNew( async () =>
			{
				await Task.Delay( random.Next( 3000, 15000 ) );
				e3.StartWashingHome();
			} ) );

			tasks.Add( new TaskFactory().StartNew( async () =>
			{
				await Task.Delay( random.Next( 17000, 31000 ) );
				e1.EndCleaningGarden();
			} ) );
			tasks.Add( new TaskFactory().StartNew( async () =>
			{
				await Task.Delay( random.Next( 17000, 31000 ) );
				e2.EndSleeping();
			} ) );
			tasks.Add( new TaskFactory().StartNew( async () =>
			{
				await Task.Delay( random.Next( 17000, 31000 ) );
				e3.EndWashingHome();
			} ) );

			Console.ReadLine();
		}
	}
}