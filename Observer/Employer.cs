﻿using System;
using System.Collections.Generic;

namespace Observer
{
	public class Employer: IObservable
	{
		private readonly string _name;
		private readonly List< IObserver > _observers;

		public Employer( string name )
		{
			if( name == null )
				throw new ArgumentNullException( "name" );
			this._name = name;

			this._observers = new List< IObserver >();
		}

		public string Name
		{
			get { return this._name; }
		}

		public void AddObserver( IObserver observer )
		{
			if( !this._observers.Contains( observer ) )
				this._observers.Add( observer );
		}

		public void RemoveObserver( IObserver observer )
		{
			if( this._observers.Contains( observer ) )
				this._observers.Remove( observer );
		}

		public void Send( WorkStarted workStarted )
		{
			foreach( var observer in this._observers )
			{
				observer.When( workStarted );
			}
		}

		public void Send( WorkEnded workEnded )
		{
			foreach( var observer in this._observers )
			{
				observer.When( workEnded );
			}
		}
	}

	public static class EployerExtensions
	{
		public static void StartWashingHome( this Employer employer )
		{
			employer.Send( new WorkStarted( "Start washing home", employer.Name ) );
		}

		public static void EndWashingHome( this Employer employer )
		{
			employer.Send( new WorkStarted( "End washing home", employer.Name ) );
		}

		public static void StartCleanGarden( this Employer employer )
		{
			employer.Send( new WorkStarted( "Start cleaning garden", employer.Name ) );
		}

		public static void EndCleaningGarden( this Employer employer )
		{
			employer.Send( new WorkStarted( "End cleaning garden", employer.Name ) );
		}

		public static void StartSleeping( this Employer employer )
		{
			employer.Send( new WorkStarted( "Start sleeping", employer.Name ) );
		}

		public static void EndSleeping( this Employer employer )
		{
			employer.Send( new WorkStarted( "End sleeping", employer.Name ) );
		}
	}
}