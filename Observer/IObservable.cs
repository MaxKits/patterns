﻿namespace Observer
{
	public interface IObservable
	{
		void AddObserver( IObserver observer );
		void RemoveObserver( IObserver observer );
		void Send( WorkStarted workStarted );
		void Send( WorkEnded workEnded );
	}

	public class WorkStarted
	{
		private readonly string _workDescription;
		private readonly string _name;

		public WorkStarted( string workDescription, string name )
		{
			if( workDescription == null )
				workDescription = string.Empty;
			if( name == null )
				name = string.Empty;
			this._workDescription = workDescription;
			this._name = name;
		}

		public string Name
		{
			get { return this._name; }
		}

		public string WorkDescription
		{
			get { return this._workDescription; }
		}
	}

	public class WorkEnded
	{
		private readonly string _workDescription;
		private readonly string _name;

		public WorkEnded( string workDescription, string name )
		{
			if( workDescription == null )
				workDescription = string.Empty;
			if( name == null )
				name = string.Empty;
			this._workDescription = workDescription;
			this._name = name;
		}

		public string Name
		{
			get { return this._name; }
		}

		public string WorkDescription
		{
			get { return this._workDescription; }
		}
	}
}