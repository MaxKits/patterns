﻿namespace Observer
{
	public interface IObserver
	{
		void When( WorkStarted workStarted );
		void When( WorkEnded workEnded );
	}
}