﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CancelOperationWithCancelationTokenSource
{
	class Program
	{
		static void Main(string[] args)
		{
			CancelOperationWithCancelationTokenSource.CancelationDemo.GoWithSimpleCancelation();
			CancelOperationWithCancelationTokenSource.CancelationDemo.GoWithOneCtsAnd2MethodsRegistered();
			CancelOperationWithCancelationTokenSource.CancelationDemo.GoWithMultipleCancelations();
		}
	}
}
