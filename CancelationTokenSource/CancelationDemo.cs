﻿using System;
using System.Threading;

namespace CancelOperationWithCancelationTokenSource
{
	internal static class CancelationDemo
	{
		/// <summary>
		/// Simple
		/// </summary>
		public static void GoWithSimpleCancelation()
		{
			Console.WriteLine(" --- Simple cancelation --- ");
			CancellationTokenSource cts = new CancellationTokenSource();

			ThreadPool.QueueUserWorkItem(o => Count(cts.Token, 100));
			Console.WriteLine("Press <Enter> to cancel oparation");
			Console.ReadLine();
			cts.Cancel();

			Console.WriteLine("Press <Enter> to exit");
			Console.ReadLine();

		}
		/// <summary>
		/// Wneg cancel occured cancel methods invoked
		/// </summary>
		public static void GoWithOneCtsAnd2MethodsRegistered()
		{
			Console.WriteLine(" --- One cancelationToken with 2 delegates registered --- ");
			CancellationTokenSource cts = new CancellationTokenSource();
			cts.Token.Register(() => Console.WriteLine("Cancel method 1"));
			cts.Token.Register(() => Console.WriteLine("Cancel method 2"));

			ThreadPool.QueueUserWorkItem(o => Count(cts.Token, 100));
			Console.WriteLine("Press <Enter> to cancel oparation");
			Console.ReadLine();
			cts.Cancel();

			Console.WriteLine("Press <Enter> to exit");
			Console.ReadLine();

		}

		public static void GoWithMultipleCancelations()
		{
			Console.WriteLine(" --- One linked cancelationTokenSource aggregates 2 cancelationTokens (cts1, cts2) --- ");
			CancellationTokenSource cts1 = new CancellationTokenSource();
			cts1.Token.Register(() => Console.WriteLine("Cancel method 1"));

			CancellationTokenSource cts2 = new CancellationTokenSource();
			cts2.Token.Register(() => Console.WriteLine("Cancel method 2"));

			var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(cts1.Token, cts2.Token);
			linkedCts.Token.Register(() => Console.WriteLine("Cancel method 3"));

			ThreadPool.QueueUserWorkItem(o => Count(cts1.Token, 100));

			Console.WriteLine("Press <Enter> to cancel throught cts1");
			Console.ReadLine();
			cts1.Cancel();

			Console.WriteLine("cts1 canceled={0}, cts1 canceled={1}, linkedCts={2}", cts1.IsCancellationRequested,
				cts2.IsCancellationRequested, linkedCts.IsCancellationRequested);

			Console.WriteLine("Press <Enter> to exit");
			Console.ReadLine();

		}

		private static void Count(CancellationToken token, int i)
		{
			for (int j = 0; j < i; j++)
			{
				if (token.IsCancellationRequested)
				{
					Console.WriteLine("Canceled");
					break;
				}

				Console.WriteLine(j);
				Thread.Sleep(200);
			}

			Console.WriteLine("Cout is done");
		}
	}
}