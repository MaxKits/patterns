﻿namespace Builder
{
	public class RichCarBuilder: ICarBuilder
	{
		private readonly Car _car;

		public RichCarBuilder()
		{
			this._car = new Car();
		}

		public void SetColour( string colour )
		{
			this._car.Colour = colour;
		}

		public void SetWheels( int count )
		{
			this._car.Wheels = count;
		}

		public void SetGps( bool setGps )
		{
		}

		public void SetHandleColors( bool colour )
		{
		}

		public Car GetResult()
		{
			return this._car;
		}
	}
}