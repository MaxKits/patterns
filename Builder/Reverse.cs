﻿using System;

namespace Builder
{
	public static class Applicator
	{
		public static Func< T2, T1, T3 > Reverse< T1, T2, T3 >( Func< T1, T2, T3 > func )
		{
			return ( x, y ) => func( y, x );
		}

		public static Func< T3, T2, T1, T4 > Reverse< T1, T2, T3, T4 >( Func< T1, T2, T3, T4 > func )
		{
			return ( x, y, z ) => func( z, y, x );
		}
	}
}