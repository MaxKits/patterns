﻿namespace Builder
{
	public class CarBuildDirector
	{
		public Car Construct()
		{
			ICarBuilder builder = new RichCarBuilder();

			builder.SetColour( "Red" );
			builder.SetWheels( 4 );

			return builder.GetResult();
		}
	}
}