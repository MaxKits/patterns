﻿namespace Builder
{
	public interface ICarBuilder
	{
		void SetColour( string colour );
		void SetWheels( int count );
		void SetGps( bool setGps );
		void SetHandleColors( bool colour );
		Car GetResult();
	}
}