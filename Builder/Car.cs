namespace Builder
{
	/// <summary>
	/// Represents a product created by the builder
	/// </summary>
	public class Car
	{
		public Car()
		{
		}

		public int Wheels{ get; set; }

		public string Colour{ get; set; }
		public bool Gps{ get; set; }
		public bool AutomaticTransmission{ get; set; }
		public bool HandleIsColored{ get; set; }
	}
}