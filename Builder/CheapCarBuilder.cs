﻿namespace Builder
{
	public class CheapCarBuilder: ICarBuilder
	{
		private readonly Car _car;

		public CheapCarBuilder()
		{
			this._car = new Car();
		}

		public void SetColour( string colour )
		{
			this._car.Colour = colour;
		}

		public void SetWheels( int count )
		{
			this._car.Wheels = count;
		}

		public void SetGps( bool setGps )
		{
			this._car.Gps = setGps;
		}

		public void SetHandleColors( bool colored )
		{
			this._car.HandleIsColored = colored;
		}

		public Car GetResult()
		{
			return this._car;
		}
	}
}