﻿using System;

namespace Builder
{
	class Program
	{
		static void Main( string[] args )
		{
			// Create director and builders
			var director = new CarBuildDirector();

			var car = director.Construct();

			Console.WriteLine( car.ToString() );
			Console.Read();
		}
	}
}