﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;

namespace MiscTools
{

	public static class EnumerableExtensions
	{
		public static async Task<IEnumerable<TResult>> ProcessInBatchAsync<TInput, TResult>(this IEnumerable<TInput> inputEnumerable, int batchSize, Func<TInput, Task<TResult>> processor, bool ignoreNull = true)
		{
			if (inputEnumerable == null)
				return Enumerable.Empty<TResult>();

			Condition.Requires(batchSize, "batchSize").IsGreaterOrEqual(1);
			Condition.Requires(processor, "processor").IsNotNull();

			var inputList = inputEnumerable as IList<TInput> ?? inputEnumerable.ToList();

			var result = new List<TResult>(inputList.Count);
			var processingTasks = new List<Task<TResult>>(batchSize);

			foreach (var input in inputList)
			{
				processingTasks.Add(processor(input));

				if (processingTasks.Count == batchSize) // batch is full, wait for completion and process
				{
					AddToList(await Task.WhenAll(processingTasks).ConfigureAwait(false), result, ignoreNull);
					processingTasks.Clear();
				}
			}
			AddToList(await Task.WhenAll(processingTasks).ConfigureAwait(false), result, ignoreNull);
			return result;
		}

		private static void AddToList<TResult>(IEnumerable<TResult> newList, List<TResult> mainList, bool ignoreNull)
		{
			var newListFiltered = (ignoreNull ? newList.Where(x => !Equals((object)x, (object)default(TResult))) : newList).ToList();
			mainList.AddRange(newListFiltered);
		}
	}
}
