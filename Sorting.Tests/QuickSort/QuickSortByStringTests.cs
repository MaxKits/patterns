﻿using System;
using System.Linq;
using NUnit.Framework;

namespace Sorting.Tests.QuickSort
{
	[ TestFixture ]
	public class QuickSortByStringTests: SorterTestsBase
	{
		[ Test, TestCaseSource( "GenerateLists" ) ]
		public void Sort_SomeList_SortedList( SortingTestCase sortingTestCase )
		{
			//A
			var sort = new Sorting.QuickSort.QuickSort();

			//A
			var result = sort.Sort( sortingTestCase.Source, ( f, s ) => string.Compare( f.MyString, s.MyString, StringComparison.InvariantCultureIgnoreCase ) );

			//A
			this.AreEquivalent( result, sortingTestCase.Source.OrderBy( x => x.MyString ) );
		}
	}
}