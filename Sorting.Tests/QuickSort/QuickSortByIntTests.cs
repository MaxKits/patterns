﻿using System.Linq;
using NUnit.Framework;

namespace Sorting.Tests.QuickSort
{
	[ TestFixture ]
	public class QuickSortByIntTests: SorterTestsBase
	{
		[ Test, TestCaseSource( "GenerateLists" ) ]
		public void Sort_SomeList_SortedList( SortingTestCase sortingTestCase )
		{
			//A
			var sort = new Sorting.QuickSort.QuickSort();

			//A
			var result = sort.Sort( sortingTestCase.Source, ( f, s ) => f.MyInt > s.MyInt ? 1 : ( f.MyInt == s.MyInt ? 0 : -1 ) );

			//A
			this.AreEquivalent( result, sortingTestCase.Source.OrderBy( x => x.MyInt ) );
		}
	}
}