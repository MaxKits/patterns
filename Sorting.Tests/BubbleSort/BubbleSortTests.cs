﻿using System.Linq;
using NUnit.Framework;

namespace Sorting.Tests.BubbleSort
{
	[ TestFixture ]
	public class BubbleSortByIntTests: SorterTestsBase
	{
		[ Test, TestCaseSource( "GenerateLists" ) ]
		public void Sort_SomeList_SortedList( SortingTestCase sortingTestCase )
		{
			//A
			var sort = new Sorting.BubbleSort.BubbleSort();

			//A
			var result = sort.Sort( sortingTestCase.Source, ( f, s ) => f.MyInt > s.MyInt ? 1 : ( f.MyInt == s.MyInt ? 0 : -1 ) );

			//A
			this.AreEquivalent( result, sortingTestCase.Source.OrderBy( x => x.MyInt ) );
		}
	}
}