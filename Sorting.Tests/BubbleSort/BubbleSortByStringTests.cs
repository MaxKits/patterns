﻿using System;
using System.Linq;
using NUnit.Framework;

namespace Sorting.Tests.BubbleSort
{
	[ TestFixture ]
	public class BubbleSortByStringTests: SorterTestsBase
	{
		[ Test, TestCaseSource( "GenerateLists" ) ]
		public void Sort_SomeList_SortedList( SortingTestCase sortingTestCase )
		{
			//A
			var sort = new Sorting.BubbleSort.BubbleSort();

			//A
			var result = sort.Sort( sortingTestCase.Source, ( f, s ) => string.Compare( f.MyString, s.MyString, StringComparison.InvariantCultureIgnoreCase ) );

			//A
			this.AreEquivalent( result, sortingTestCase.Source.OrderBy( x => x.MyString ) );
		}
	}
}