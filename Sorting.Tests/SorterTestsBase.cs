﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace Sorting.Tests
{
	public class MyFactoryClass
	{
		public static IEnumerable TestCases
		{
			get
			{
				var concurentList = new ConcurrentStack< SortingTestCase >();
				var cases = new List< Tuple< int, int > >()
				{
					Tuple.Create( 5000, -1 ),
					Tuple.Create( 5000, 0 ),
					Tuple.Create( 5000, 1 ),
					Tuple.Create( 5000, 2 ),
				};

				Parallel.ForEach( cases, x => concurentList.Push( SorterTestsBase.GenerateList( x.Item1, x.Item2 ) ) );
				foreach( var sortingTestCase in concurentList )
				{
					yield return new TestCaseData( sortingTestCase );
				}
				concurentList.ToList();
			}
		}
	}

	public class SorterTestsBase
	{
		/// <summary>
		/// Lists generator
		/// </summary>
		/// <param name="length">Output list length</param>
		/// <param name="order">-1 if list should be ordered by desc, 0 if all elements are equal, 1 if list should be ordered, else random list</param>
		/// <returns></returns>
		public static SortingTestCase GenerateList( int length, int order )
		{
			var outputList = new MyData[ length ];
			if( order == -1 )
			{
				for( var i = 0; i < length; i++ )
				{
					var elementValue = length - i;
					outputList[ i ] = new MyData() { MyInt = elementValue, MyString = elementValue.ToString() };
				}
			}
			else if( order == -1 )
			{
				for( var i = 0; i < length; i++ )
				{
					outputList[ i ] = new MyData() { MyInt = i, MyString = i.ToString() };
				}
			}
			else if( order == 0 )
			{
				for( var i = 0; i < length; i++ )
				{
					outputList[ i ] = new MyData() { MyInt = length, MyString = length.ToString() };
				}
			}
			else
			{
				var r = new Random( 100 );
				for( var i = 0; i < length; i++ )
				{
					var elementValue = r.Next( length / 2 );
					outputList[ i ] = new MyData() { MyInt = elementValue, MyString = elementValue.ToString() };
				}
			}
			return ( new SortingTestCase() { Order = order, Source = outputList } );
		}

		public static IEnumerable< TestCaseData > GenerateLists()
		{
			var concurentList = new ConcurrentStack< TestCaseData >();
			var cases = new List< Tuple< int, int, string > >()
			{
				Tuple.Create( 5000, -1, "OrderedByDesc" ),
				Tuple.Create( 5000, 0, "AllElementsAreEqual" ),
				Tuple.Create( 5000, 1, "Ordered" ),
				Tuple.Create( 5000, 2, "Random" ),
			};

			Parallel.ForEach( cases, x => concurentList.Push( new TestCaseData( GenerateList( x.Item1, x.Item2 ) ).SetName( x.Item3 ) ) );

			return concurentList.ToList();
		}

		protected void AreEquivalent( IEnumerable< MyData > s1, IEnumerable< MyData > s2 )
		{
			var s1List = s1 as IList< MyData > ?? s1.ToList();
			var s2list = s2 as IList< MyData > ?? s2.ToList();

			var myInts_s1 = s1List.Select( x => x.MyInt );
			var myInts_s2 = s2list.Select( x => x.MyInt );
			myInts_s1.Should().Equal( myInts_s2 );

			var myStrings_s1 = s1List.Select( x => x.MyString );
			var myStrings_s2 = s2list.Select( x => x.MyString );
			myStrings_s1.Should().Equal( myStrings_s2 );
		}
	}

	public class SortingTestCase
	{
		public int Order{ get; set; }
		public IEnumerable< MyData > Source{ get; set; }
	}
}