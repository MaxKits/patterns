﻿using System.Linq;
using NUnit.Framework;

namespace Sorting.Tests
{
	[ TestFixture ]
	public class TestSortingTestsByString: SorterTestsBase
	{
		[ Test, TestCaseSource( "GenerateLists" ) ]
		public void TestsForTests_SomeListByString_AlwaysTrue( SortingTestCase sortingTestCase )
		{
			//A

			//A
			var result = sortingTestCase.Source.OrderBy( x => x.MyString );

			//A
			this.AreEquivalent( result, sortingTestCase.Source.OrderBy( x => x.MyString ) );
		}
	}
}