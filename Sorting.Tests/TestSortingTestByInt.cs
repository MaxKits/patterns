﻿using System.Linq;
using NUnit.Framework;

namespace Sorting.Tests
{
	[ TestFixture ]
	public class TestSortingTestByInt: SorterTestsBase
	{
		[ Test, TestCaseSource( "GenerateLists" ) ]
		public void TestsForTests_SomeList_AlwaysTrue( SortingTestCase sortingTestCase )
		{
			//A

			//A
			var result = sortingTestCase.Source.OrderBy( x => x.MyInt );

			//A
			this.AreEquivalent( result, sortingTestCase.Source.OrderBy( x => x.MyInt ) );
		}
	}
}