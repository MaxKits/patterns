﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.


[<EntryPoint>]
let main argv = 
    let r = TailRecursion.Programm
    if r.is
    let rse = Seq.head(TailRecursion.GenerateOrderLineRecord)
    printfn "%A" rse
    printfn "%A" argv
    System.Console.ReadLine()
    0 // return an integer exit code
