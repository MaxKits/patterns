﻿module TailRecursion

type OrderLineRecord = {Sku:string;Qty:int;Price:decimal; mutable Description:string} 
type OrderRecord = {Line:System.Collections.Generic.IEnumerable<OrderLineRecord>} 

let GenerateOrderLineRecord =  seq { for i in 50 .. 100 ->  {Sku=i.ToString(); Qty=i; Price=(decimal)i;Description=""} }
let GenerateOrder =  {Line = seq { for i in 1 .. 100 ->  {Sku=i.ToString(); Qty=i; Price=(decimal)i;Description=i.ToString()} }}

let predicate1 (x:OrderLineRecord) (y:OrderLineRecord) =  x.Sku = y.Sku
let predicate2 (x:OrderLineRecord) (y:OrderLineRecord) = predicate1 x y && x.Qty = y.Qty

//let funList = [ predicate1; predicate2 ]
let funList1 = seq { for i in 1..1 -> fun (x:OrderLineRecord) (y:OrderLineRecord) -> x.Sku = y.Sku }
let funList2 = seq { for i in 1..1 -> fun (x:OrderLineRecord) (y:OrderLineRecord) -> x.Sku = y.Sku }

let funList = Seq.append funList1 funList2

let rec TryFindSimilarItemAndSetDescription twinkOrder orderLine (predicate:seq<(OrderLineRecord ->OrderLineRecord -> bool )>) = 
    if predicate = null then None
    elif Seq.isEmpty predicate then None
    else twinkOrder.Line 
            |> Seq.filter (fun x ->(Seq.head predicate)) x orderLine
    
    
    
    
    
     //Seq.filter (fun x ->(Seq.head predicate) x orderLine) >> fun x ->  if Seq.length ==1 then Seq.head >>  if Seq.length x = 1 then Seq.head x elif Seq.length x > 1 then TryFindSimilarItemAndSetDescription twinkOrder  orderLine  (Seq.skip 1 predicate) else None >> twinkOrder.Line


let Programm = TryFindSimilarItemAndSetDescription GenerateOrder (Seq.head GenerateOrderLineRecord) funList 


