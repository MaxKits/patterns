﻿using System;

namespace AzureClient
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			//don't forget to use correct time on your local machine;
			var blobReader = BlobManager.CreateDefault();
			blobReader.WriteStringUTF8( "Hello World!" );
			var readedStr = blobReader.ReadString();
			Console.WriteLine( readedStr );
			Console.ReadLine();
		}
	}
}