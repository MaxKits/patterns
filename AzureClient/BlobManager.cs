﻿using System.Configuration;
using System.IO;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace AzureClient
{
	public class BlobManager
	{
		protected readonly CloudBlockBlob _blockBlob;

		public BlobManager( string containerName, string blob )
		{
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
				ConfigurationManager.AppSettings[ "StorageConnectionString" ] );

			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

			CloudBlobContainer container = blobClient.GetContainerReference( containerName );
			container.CreateIfNotExists();

			this._blockBlob = container.GetBlockBlobReference( blob );
		}

		public static BlobManager CreateDefault()
		{
			var b = new BlobManager( "defaultcontainer", "defaultblob" );
			return b;
		}

		public void WriteStringUTF8( string str )
		{
			this.Write( Encoding.UTF8.GetBytes( str ) );
		}

		public void Write( byte[] bytes )
		{
			var v = new MemoryStream( bytes );
			using( v )
				this._blockBlob.UploadFromStream( v );
		}

		public string ReadString()
		{
			return Encoding.UTF8.GetString( this.ReadBytes() );
		}

		public byte[] ReadBytes()
		{
			byte[] res;
			var v = new MemoryStream();
			using( v )
			{
				this._blockBlob.DownloadToStream( v );
				res = v.ToArray();
			}
			return res;
		}
	}
}