﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ComputerAssembler.cs" company="MaxSoft" author="MKitsenko">
//       Copyright (c) 02.12.2013. All rights reserved.
//  </copyright>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using Factory.abstr;

namespace Factory
{
	public class ComputerAssembler
	{
		public void AssembleComputer( ComputerFactory factory )
		{
			Computer computer = factory.GetComputer();
			Console.WriteLine( "assembled a {0} running at {1} MHz",
				computer.GetType().FullName, computer.Mhz );
		} //AssembleComputer
	} //ComputerAssembler
}