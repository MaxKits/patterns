//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ComputerFactory.cs" company="MaxSoft" author="MKitsenko">
//       Copyright (c) 02.12.2013. All rights reserved.
//  </copyright>
//  --------------------------------------------------------------------------------------------------------------------

namespace Factory.abstr
{
	public abstract class ComputerFactory
	{
		/// <summary>
		/// It is a factory method
		/// </summary>
		/// <returns></returns>
		public abstract Computer GetComputer();
	}
}