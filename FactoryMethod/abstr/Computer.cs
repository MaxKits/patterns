//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="Computer.cs" company="MaxSoft" author="MKitsenko">
//       Copyright (c) 02.12.2013. All rights reserved.
//  </copyright>
//  --------------------------------------------------------------------------------------------------------------------

namespace Factory.abstr
{
	public abstract class Computer
	{
		public abstract int Mhz{ get; }
	}
}