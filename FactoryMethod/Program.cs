﻿using System;
using Factory.abstr;
using Factory.concr;

namespace Factory
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			////////////// example of classicafactory method /////////////////////////////////////////
			/// 
			//ComputerFactory factory = new ConcreteComputerFactoryA();
			//new ComputerAssembler().AssembleComputer(factory);

			// an array of creators
			ComputerFactory[] creators = { new ConcreteComputerFactoryA(), new ConcreteComputerFactoryB() };

			// iterate over creators and create products
			foreach( ComputerFactory creator in creators )
			{
				Computer product = creator.GetComputer();
				Console.WriteLine( "Created {0}", product.GetType() );
			}

			/////////////////// example of paramtrizedfactory method /////////////////////////////

			var computerFactoryParametrised = new ComputerFactoryParametrised();
			var comp1 = computerFactoryParametrised.GetComputer( "A" );
			var comp2 = computerFactoryParametrised.GetComputer( "B" );

			Console.WriteLine( "Press any key" );
			Console.ReadLine();
		}
	}
}