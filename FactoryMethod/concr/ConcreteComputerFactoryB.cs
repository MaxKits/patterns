//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ConcreteComputerFactoryA.cs" company="MaxSoft" author="MKitsenko">
//       Copyright (c) 02.12.2013. All rights reserved.
//  </copyright>
//  --------------------------------------------------------------------------------------------------------------------

using Factory.abstr;

namespace Factory.concr
{
	public class ConcreteComputerFactoryB: ComputerFactory
	{
		public override Computer GetComputer()
		{
			return new ConcreteComputerB();
		} //GetComputer
	} //ConcreteComputerFactoryA
}