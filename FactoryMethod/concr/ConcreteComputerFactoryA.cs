//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ConcreteComputerFactoryA.cs" company="MaxSoft" author="MKitsenko">
//       Copyright (c) 02.12.2013. All rights reserved.
//  </copyright>
//  --------------------------------------------------------------------------------------------------------------------

using Factory.abstr;

namespace Factory.concr
{
	public class ConcreteComputerFactoryA: ComputerFactory
	{
		public override Computer GetComputer()
		{
			return new ConcreteComputerA();
		} //GetComputer
	} //ConcreteComputerFactoryA
}