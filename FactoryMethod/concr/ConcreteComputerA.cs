//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ConcreteComputer.cs" company="MaxSoft" author="MKitsenko">
//       Copyright (c) 02.12.2013. All rights reserved.
//  </copyright>
//  --------------------------------------------------------------------------------------------------------------------

using Factory.abstr;

namespace Factory.concr
{
	public class ConcreteComputerA: Computer
	{
		private readonly int _mhz = 500;

		public override int Mhz
		{
			get { return this._mhz; }
		} //Mhz
	} //ConcreteComputer
}