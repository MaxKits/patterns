//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ConcreteComputerFactoryA.cs" company="MaxSoft" author="MKitsenko">
//       Copyright (c) 02.12.2013. All rights reserved.
//  </copyright>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using Factory.abstr;

namespace Factory.concr
{
	public class ComputerFactoryParametrised
	{
		public Computer GetComputer( string what )
		{
			switch( what )
			{
				case "A":
					return new ConcreteComputerA();
				case "B":
					return new ConcreteComputerB();
				default:
					throw new Exception();
			}
		}
	}
}