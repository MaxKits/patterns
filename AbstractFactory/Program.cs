﻿using System;
using AbstractFactory.Toyota;

namespace AbstractFactory
{
	class Program
	{
		static void Main( string[] args )
		{
			CompileCar();
			Console.ReadLine();
		}

		private static void CompileCar()
		{
			AbstractFactory f = new ToyotaFactory();

			var e = f.CreateEngine();
			var t = f.CreateTransmission();

			Console.WriteLine( e.ToString() );
			Console.WriteLine( t.ToString() );
		}
	}
}