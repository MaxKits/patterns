﻿namespace AbstractFactory
{
	public abstract class AbstractFactory
	{
		public abstract AbstractEngine CreateEngine();
		public abstract AbstractTransmission CreateTransmission();
	}

	public abstract class AbstractEngine
	{
		public abstract bool StartEngine( AbstractTransmission transmission );
	}

	public abstract class AbstractTransmission
	{
		public abstract string Speed{ get; set; }
	}
}