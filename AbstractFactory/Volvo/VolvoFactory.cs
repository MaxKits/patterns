﻿namespace AbstractFactory.Volvo
{
	public class VolvoFactory: AbstractFactory
	{
		public override AbstractEngine CreateEngine()
		{
			return new Engine();
		}

		public override AbstractTransmission CreateTransmission()
		{
			return new Transmission();
		}
	}
}