﻿namespace AbstractFactory.Volvo
{
	public class Transmission: AbstractTransmission
	{
		public override string Speed{ get; set; }
	}
}