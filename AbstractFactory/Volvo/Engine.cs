﻿using System;

namespace AbstractFactory.Volvo
{
	public class Engine: AbstractEngine
	{
		public override bool StartEngine( AbstractTransmission transmission )
		{
			return string.Compare( transmission.Speed, "s", StringComparison.InvariantCultureIgnoreCase ) != 0;
		}
	}
}