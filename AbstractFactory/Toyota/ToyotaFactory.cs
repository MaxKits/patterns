﻿namespace AbstractFactory.Toyota
{
	public class ToyotaFactory: AbstractFactory
	{
		public override AbstractEngine CreateEngine()
		{
			return new Engine();
		}

		public override AbstractTransmission CreateTransmission()
		{
			return new Transmission();
		}
	}
}