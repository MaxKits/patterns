﻿using System;

namespace AbstractFactory.Toyota
{
	public class Engine: AbstractEngine
	{
		public override bool StartEngine( AbstractTransmission transmission )
		{
			return string.Compare( transmission.Speed, "r", StringComparison.InvariantCultureIgnoreCase ) != 0;
		}
	}
}