﻿namespace AbstractFactory.Toyota
{
	public class Transmission: AbstractTransmission
	{
		public override string Speed{ get; set; }
	}
}