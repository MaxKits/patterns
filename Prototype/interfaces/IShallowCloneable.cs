﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="IShallowCloneable.cs" company="MaxSoft" author="MKitsenko">
//       Copyright (c) 17.09.2013. All rights reserved.
//  </copyright>
//  --------------------------------------------------------------------------------------------------------------------

namespace Prototype.interfaces
{
	public interface IShallowCloneable< out T >
	{
		T ShallowClone();
	}
}