﻿using System;

namespace Prototype
{
	internal class Program
	{
		private readonly Test1 _test1 = new Test1();

		private static void Main( string[] args )
		{
			Test1 t = new Test1();
			t.Init();
			t.TestShallowCapitanNumber();
			t.Init();
			t.TestShallowObjectCountry();
			t.Init();
			t.TestShallowObjectPlayer();
			t.Init();
			t.TestShallowPlayerName();
			
			t.Init();
			t.TestDeepCapitanNumber();
			t.Init();
			t.TestDeepObjectCountry();
			t.Init();
			t.TestDeepObjectPlayer();
			t.Init();
			t.TestDeepPlayerName();

			Console.ReadLine();
		}
	}
}