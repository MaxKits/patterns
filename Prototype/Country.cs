﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="Country.cs" company="MaxSoft" author="MKitsenko">
//       Copyright (c) 17.09.2013. All rights reserved.
//  </copyright>
//  --------------------------------------------------------------------------------------------------------------------

using System;

namespace Prototype
{
	[ Serializable ]
	public class Country: BaseCloneable< Player >
	{
		public string Name{ get; set; }

		public int Population{ get; set; }

		public override string ToString()
		{
			return string.Format( "{0}(Population: {1}), HashCode: {2}", this.Name, this.Population, this.GetHashCode() );
		}
	}
}