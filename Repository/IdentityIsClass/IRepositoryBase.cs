﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.IdentityIsClass
{
	public interface IRepositoryBase<TEntity, in TId>
		where TEntity : class
		where TId : class
	{
		IEnumerable<TEntity> Get(Expression<Func< TEntity, bool > > filter = null,
			Func<IQueryable< TEntity >, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");

		TEntity GetByID(TId id, bool detach = false);

		void Insert(TEntity entity);

		void Delete(TId id);

		void Delete(TEntity entityToDelete);

		void Update(TEntity entityToUpdate);
	}
}