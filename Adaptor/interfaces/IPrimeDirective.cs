﻿namespace Adaptor
{
	/// <summary>
	/// New interface
	/// </summary>
	public interface IPrimeDirective
	{
		int FindNearestPrime( int number );
	}
}