﻿namespace Adaptor
{
	/// <summary>
	/// Provides a contract for adapting to the IPrimeDirective interface
	/// </summary>
	public interface IPrimeDirectiveAdapter: IPrimeDirective
	{
		// todo: kill this
		//int FindNearestPrime(int number);
	}
}