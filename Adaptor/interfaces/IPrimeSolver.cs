﻿namespace Adaptor
{
	/// <summary>
	/// Legacy old Interface
	/// </summary>
	public interface IPrimeSolver
	{
		int CalculatePrime( int number );
	}
}