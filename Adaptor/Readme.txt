﻿The Adapter Pattern allows incompatible interfaces to communicate.The Adapter Pattern is a common software design pattern that allows you to adapt one interface into another interface that the client expects. 


The pattern is also great for wrapping hard-to-test code to increase your code coverage.