﻿using System;

namespace Adaptor
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			var primeDirective = new PrimeSolverToPrimeDirectiveAdapter();

			int num;
			Console.WriteLine( "Enter a number to find its nearest prime:" );
			int.TryParse( Console.ReadLine(), out num );
			int result = primeDirective.FindNearestPrime( num );
			Console.WriteLine( "The nearest prime to {0} is {1}.", num, result );
		}
	}
}