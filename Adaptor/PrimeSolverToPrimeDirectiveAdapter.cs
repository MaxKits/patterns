﻿namespace Adaptor
{
	/// <summary>
	/// PrimeSolverToPrimeDirectiveAdapter class that adapts the old 
	/// IPrimeSolver interface into the new IPrimeDirective interface
	/// </summary>
	internal class PrimeSolverToPrimeDirectiveAdapter: IPrimeDirectiveAdapter
	{
		/// <summary>
		///  Adapter class looks like the "right" class to the client: the client 
		/// instantiates the Adapter class and just uses the members of the 
		/// interface that the client wants. The Adapter class hides all the 
		/// ugly details, including instantiating the class that requires adapting.
		/// </summary>
		private readonly PrimeSolver _primeSolver;

		public PrimeSolverToPrimeDirectiveAdapter()
		{
			this._primeSolver = new PrimeSolver();
		}

		public int FindNearestPrime( int number )
		{
			return this._primeSolver.CalculatePrime( number );
		}
	}
}