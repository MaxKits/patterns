// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpaceCharactersValidator.cs" company="SofTrust" author="MKitsenko">
//      Copyright (c) 2013. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq.Expressions;
using Specifications.Specifications.abstr;

namespace Specifications.specifications.concrete
{
	public class IsSquare: Specification< Rectangle >
	{
		/// <summary>
		/// �������� ��� �������� ������������
		/// </summary>
		protected override Expression< Func< Rectangle, bool > > Predicate
		{
			get { return ( Rectangle p ) => this.someFunc( p ); }
		}

		private bool someFunc( Rectangle p )
		{
			if( p.X == p.Y )
				return true;
			else
				return false;
		}
	}
}