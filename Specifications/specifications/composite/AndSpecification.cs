// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AndSpecification.cs" company="SofTrust" author="MKitsenko">
//      Copyright (c) 2013. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Linq.Expressions;
using Specifications.Expressions;
using Specifications.Specifications.abstr;

namespace Specifications.Specifications.Composite
{
	/// <summary>
	/// ����������� ������������ �� �
	/// </summary>
	/// <typeparam name="T">��� �������, ��� �������� ����������� ������������</typeparam>
	internal class AndSpecification< T >: CompositeSpecification< T >
	{
		/// <summary>
		/// �����������
		/// </summary>
		public AndSpecification()
		{
		}

		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="specifications">������ ������������</param>
		public AndSpecification( params Specification< T >[] specifications )
			: base( specifications )
		{
		}

		/// <summary>
		/// �������� ��� �������� ������������
		/// </summary>
		protected override Expression< Func< T, bool > > Predicate
		{
			get
			{
				Expression< Func< T, bool > > result = this.Specifications.First();
				return this.Specifications.Skip( 1 ).Aggregate( result, ( current, specification ) => current.And( ( Expression< Func< T, bool > > )specification ) );
			}
		}
	}
}