﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotSpecification.cs" company="SofTrust" author="MKitsenko">
//      Copyright (c) 2013. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq.Expressions;
using Specifications.Expressions;
using Specifications.Specifications.abstr;

namespace Specifications.Specifications.Composite
{
	/// <summary>
	/// Инверсия спецификации
	/// </summary>
	/// <typeparam name="T">Тип объекта, для которого применяется спецификация</typeparam>
	internal class NotSpecification< T >: Specification< T >
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="specification">Спецификация</param>
		/// <exception cref="ArgumentNullException"/>
		public NotSpecification( Specification< T > specification )
		{
			if( specification == null )
				throw new ArgumentNullException();

			this._specification = specification;
		}

		private readonly Specification< T > _specification;

		/// <summary>
		/// Спецификация
		/// </summary>
		public Specification< T > Specification
		{
			get { return this._specification; }
		}

		/// <summary>
		/// Предикат для проверки спецификации
		/// </summary>
		protected override Expression< Func< T, bool > > Predicate
		{
			get { return ( ( Expression< Func< T, bool > > )this._specification ).Not(); }
		}
	}
}