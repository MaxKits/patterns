﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrSpecification.cs" company="SofTrust" author="MKitsenko">
//      Copyright (c) 2013. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Linq.Expressions;
using Specifications.Expressions;
using Specifications.Specifications.abstr;

namespace Specifications.Specifications.Composite
{
	/// <summary>
	/// Объединение спецификаций по ИЛИ
	/// </summary>
	/// <typeparam name="T">Тип объекта, для которого применяется спецификация</typeparam>
	internal class OrSpecification< T >: CompositeSpecification< T >
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		public OrSpecification()
		{
		}

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="specifications">Список спецификаций</param>
		public OrSpecification( params Specification< T >[] specifications )
			: base( specifications )
		{
		}

		/// <summary>
		/// Предикат для проверки спецификации
		/// </summary>
		protected override Expression< Func< T, bool > > Predicate
		{
			get
			{
				Expression< Func< T, bool > > result = this.Specifications.First();
				return this.Specifications.Skip( 1 ).Aggregate( result, ( current, specification ) => current.Or( ( Expression< Func< T, bool > > )specification ) );
			}
		}
	}
}