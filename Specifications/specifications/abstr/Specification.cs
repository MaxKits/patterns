﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Specification.cs" company="SofTrust" author="MKitsenko">
//      Copyright (c) 2013. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq.Expressions;
using Specifications.Specifications.Composite;

namespace Specifications.Specifications.abstr
{
	/// <summary>
	/// Базовый класс спецификации
	/// </summary>
	/// <typeparam name="T">Тип объекта, для которого применяется спецификация</typeparam>
	public abstract class Specification< T >
	{
		/// <summary>
		/// Удовлетворяет ли объект спецификации
		/// </summary>
		/// <param name="item">Проверяемый объект</param>
		/// <returns>todo:change
		/// </returns>
		public bool IsSatisfiedBy( T item )
		{
			return this.Predicate.Compile()( item );
		}

		/// <summary>
		/// Предикат для проверки спецификации
		/// </summary>
		protected abstract Expression< Func< T, bool > > Predicate{ get; }

		/// <summary>
		/// Not
		/// </summary>
		/// <param name="specification"></param>
		/// <returns>bool</returns>
		public static Specification< T > operator !( Specification< T > specification )
		{
			return new NotSpecification< T >( specification );
		}

		/// <summary>
		/// Or
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns>bool</returns>
		public static Specification< T > operator |( Specification< T > left, Specification< T > right )
		{
			return new OrSpecification< T >( left, right );
		}

		/// <summary>
		/// And
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns>bool</returns>
		public static Specification< T > operator &( Specification< T > left, Specification< T > right )
		{
			return new AndSpecification< T >( left, right );
		}

		/// <summary>
		/// todo:change
		/// </summary>
		/// <param name="specification"></param>
		/// <returns>todo:change</returns>
		public static implicit operator Predicate< T >( Specification< T > specification )
		{
			return specification.IsSatisfiedBy;
		}

		/// <summary>
		/// todo:change
		/// </summary>
		/// <param name="specification"></param>
		/// <returns>todo:change</returns>
		public static implicit operator Func< T, bool >( Specification< T > specification )
		{
			return specification.IsSatisfiedBy;
		}

		/// <summary>
		/// todo:change
		/// </summary>
		/// <param name="specification"></param>
		/// <returns>todo:change</returns>
		public static implicit operator Expression< Func< T, bool > >( Specification< T > specification )
		{
			return specification.Predicate;
		}
	}
}