﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator.cs" company="SofTrust" author="MKitsenko">
//      Copyright (c) 2013. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq.Expressions;

namespace Specifications.Specifications.abstr
{
	public abstract class Validator< T >: Specification< T >
	{
		/// <summary>
		/// Предикат для проверки спецификации
		/// </summary>
		protected override Expression< Func< T, bool > > Predicate
		{
			get { return ( T p ) => this.someFunc( p ); }
		}

		protected abstract bool someFunc( T p );
	}
}