﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CompositeSpecification.cs" company="SofTrust" author="MKitsenko">
//      Copyright (c) 2013. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace Specifications.Specifications.abstr
{
	/// <summary>
	/// Композитная спецификация
	/// </summary>
	/// <typeparam name="T">Тип объекта, для которого применяется спецификация</typeparam>
	public abstract class CompositeSpecification< T >: Specification< T >
	{
		/// <summary>
		/// Список спецификаций
		/// </summary>
		protected readonly List< Specification< T > > Specifications;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="specifications">Список спецификаций</param>
		protected CompositeSpecification( params Specification< T >[] specifications )
		{
			this.Specifications = new List< Specification< T > >( specifications );
		}

		/// <summary>
		/// Добавить спецификацию
		/// </summary>
		/// <param name="specification">Спецификация</param>
		public void Add( Specification< T > specification )
		{
			this.Specifications.Add( specification );
		}

		/// <summary>
		/// Удалить спецификацию
		/// </summary>
		/// <param name="specification">Спецификация</param>
		public void Remove( Specification< T > specification )
		{
			this.Specifications.Remove( specification );
		}
	}
}