﻿namespace Specifications
{
	public class Rectangle
	{
		public int X{ get; set; }
		public int Y{ get; set; }
	}
}