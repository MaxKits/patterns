﻿using System;
using Specifications.specifications.concrete;

namespace Specifications
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			var exp = new IsBig() & new IsSquare();
			var r = new Rectangle() { X = 11, Y = 11 };
			var res = exp.IsSatisfiedBy( r );

			Console.WriteLine( "IsBig() & IsSquare(): {0}", res );
			Console.ReadLine();
		}
	}
}