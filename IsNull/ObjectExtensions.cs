﻿namespace IsNull
{
	public static class ObjectExtensions
	{
		public static bool IsNull( this object obj )
		{
			return obj == null;
		}

		public static bool IsNullSpecific( this INullCheckable obj )
		{
			return obj == null;
		}
	}
}