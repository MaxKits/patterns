﻿using System;

namespace IsNull
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			Object objA = null;
			Console.WriteLine( "Is null:{0}", objA.IsNull() );

			INullCheckable objB = null;
			Console.WriteLine( "Is null:{0}", objB.IsNull() );
			Console.WriteLine( "Is null:{0}", objB.IsNullSpecific() );

			Console.ReadLine();
		}
	}
}