Singleton

# Simple thread-safe singleton
Double check doesn't work in Java, and sometimes called as antipattern
**Pros**
* Thread-Safity
**Cons**
* Doesn't work in Java
* If you decide to remove double check (to use it in Java for example) - threads locked each time when they try to get singleton



# ThreadSafeWithoutLock
To avoid lock, static modifier used.
**Pros**
* Thread-Safity
* Lazy
**Cons**
* The code is a bit more complicated


# ThreadSafeWithoutLockLazy1
Instantiation is triggered by the first reference to the static member of the nested class, which only occurs in Instance. This means the implementation is fully lazy, but has all the performance benefits. Nested classes have access to the enclosing class's private members, the reverse is not true, hence the need for instance to be internal here. That doesn't raise any other problems, though, as the class itself is private. The code is a bit more complicated in order to make the instantiation lazy, however.
**Pros**
* Thread-Safity
* Lazy
**Cons**
* The code is a bit more complicated



# ThreadSafeWithoutLockLazy2
For .NET 4 (or higher), you can use the System.Lazy<T> type to make the laziness really simple. You should pass a delegate to the constructor which calls the Singleton constructor - which is done most easily with a lambda expression.
**Pros**
* Thread-Safity
* Lazy
**Cons**
* Require .net 4.0