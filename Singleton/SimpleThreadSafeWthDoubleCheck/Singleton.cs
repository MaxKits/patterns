﻿namespace Singleton.SimpleThreadSafeWthDoubleCheck
{
	public class Singleton
	{
		private static Singleton _instance;
		private static readonly object _lockObj = new object();

		private Singleton()
		{
		}

		public static Singleton Get()
		{
			if( _instance != null )
			{
				lock( _lockObj )
				{
					return _instance ?? ( _instance = new Singleton() );
				}
			}
			return _instance;
		}
	}
}