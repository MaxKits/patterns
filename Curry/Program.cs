﻿using System;

namespace Curry
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			new CarryOnLambdas().Approach();
			new CarryOnDelegates().Approach();
			Console.ReadLine();
		}
	}
}