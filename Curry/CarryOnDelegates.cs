﻿using System;

namespace Curry
{
	public class CarryOnDelegates
	{
		private float _p1;
		private float _p2;
		private float _p3;
		private float _p4;
		private float _p5;

		private float SomeCalculations( float u, float v, float x, float y, float z )
		{
			return u * v / x + y - z;
		}

		private float F5( float z )
		{
			this._p5 = z;
			return this.SomeCalculations( this._p1, this._p2, this._p3, this._p4, this._p5 );
		}

		private D5 F4( float y )
		{
			this._p4 = y;
			return this.F5;
		}

		private D4 F3( float x )
		{
			this._p3 = x;
			return this.F4;
		}

		private D3 F2( float v )
		{
			this._p2 = v;
			return this.F3;
		}

		private D2 F1( float u )
		{
			this._p1 = u;
			return this.F2;
		}

		private D1 Carry( Func< float, float, float, float, float, float > func )
		{
			D1 d1 = this.F1;
			return d1;
		}

		public void Approach()
		{
			const int arg1 = 2;
			const int arg2 = 6;
			const int arg3 = 4;
			const int arg4 = 20;
			const int arg5 = 10;

			Console.WriteLine( string.Format( "f(x1,x2,x3,x4,x5) = u*v/x + y - z" ) );
			Console.WriteLine( "f({0},{1},{2},{3},{4}) = {5}", arg1, arg2, arg3, arg4, arg5,
				this.SomeCalculations( arg1, arg2, arg3, arg4, arg5 ) );

			Console.WriteLine( string.Format( "Carry( f(x1,x2,x3,x4,x5) ) ---> f(x1)->f(x2)->f(x3)->f(x4)->f(x5)" ) );
			D1 carredFunction = this.Carry( this.SomeCalculations );
			Console.WriteLine( "f({0})({1})({2})({3})({4}) = {5}", arg1, arg2, arg3, arg4, arg5,
				carredFunction( arg1 )( arg2 )( arg3 )( arg4 )( arg5 ) );

			Type t1 = carredFunction.GetType();
			Console.WriteLine( "Carred function type {0}:", t1 );
		}

		private delegate D2 D1( float p );

		private delegate D3 D2( float p );

		private delegate D4 D3( float p );

		private delegate D5 D4( float p );

		private delegate float D5( float p );
	}
}