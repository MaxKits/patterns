﻿using System;

namespace Curry
{
	/// Curry it is an way to convert n argument function to chain of calls of n functions 
	/// each one takes 1 argument and returns function, except the last that executes result
	/// In Patterns context Curry (or Curring) it is method's factory
	public class CarryOnLambdas
	{
		private float SomeCalculations( float u, float v, float x, float y, float z )
		{
			return u * v / x + y - z;
		}

		private Func< float, Func< float, Func< float, Func< float, Func< float, float > > > > > Carry(
			Func< float, float, float, float, float, float > func )
		{
			return u => v => x => y => z => func( u, v, x, y, z );
		}

		public void Approach()
		{
			const int arg1 = 2;
			const int arg2 = 6;
			const int arg3 = 4;
			const int arg4 = 20;
			const int arg5 = 10;

			Console.WriteLine( string.Format( "f(x1,x2,x3,x4,x5) = u*v/x + y - z" ) );
			Console.WriteLine( "f({0},{1},{2},{3},{4}) = {5}", arg1, arg2, arg3, arg4, arg5,
				this.SomeCalculations( arg1, arg2, arg3, arg4, arg5 ) );

			Console.WriteLine( string.Format( "Carry( f(x1,x2,x3,x4,x5) ) ---> f(x1)->f(x2)->f(x3)->f(x4)->f(x5)" ) );
			Func< float, Func< float, Func< float, Func< float, Func< float, float > > > > > carredFunction = this.Carry( this.SomeCalculations );
			Console.WriteLine( "f({0})({1})({2})({3})({4}) = {5}", arg1, arg2, arg3, arg4, arg5,
				carredFunction( arg1 )( arg2 )( arg3 )( arg4 )( arg5 ) );

			Type t1 = carredFunction.GetType();
			Console.WriteLine( "Carred function type {0}:", t1 );
		}
	}
}